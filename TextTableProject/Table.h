#pragma once

#include <windows.h>
#include <string>

using std::string;

class Table
{
private:
	int rowsCount = 10;
	int columsCount = 4;
	
	int windowHeight;
	int windowWidth;

	string lines[20] = { "Lorem ipsum dolor sit amet,consectetur adipiscing elit.",
		"Pellentesque quis nulla id orci malesuada porta posuere quis massa.",
		"Nunc vitae pururs non augue scelerisque ultricies vitae et velit.",
		"Sed vitae lectus id sem lobortis scelerisque.",
		"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		"Praesent eget consequat lidero",
		"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
		"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
	};

public:
	void Draw(HDC hdc);
	void DrawColums(HDC hdc, const int height);
	void SetWindowSize(int height, int width);
	void DrawLine(HDC hdc, int x1, int y1, int x2, int y2, LPPOINT lppt);
};

