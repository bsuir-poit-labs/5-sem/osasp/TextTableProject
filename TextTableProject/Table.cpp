#include "Table.h"

void Table::Draw(HDC hdc)
{
	int y = 0;
	RECT rect;
	int columnWidth = windowWidth / columsCount;
	int k = 0;

	for (int i = 0; i < rowsCount; i++)
	{
		int maxStrHeight = 0;
		for (int j = 0; j < columsCount; j++)
		{
			LPCSTR str = lines[k % 8].c_str();
			int length = lines[k % 8].length();
			k++;

			SetRect(&rect, j * columnWidth + 1, y + 1, (j + 1) * columnWidth - 1, windowHeight);
			int strHeight = DrawTextA(hdc, str, length, &rect, DT_WORDBREAK);
			if (strHeight > maxStrHeight) {
				maxStrHeight = strHeight;
			}
		}
		y += maxStrHeight;
		DrawLine(hdc, 0, y, windowWidth, y, NULL);
	}

	DrawColums(hdc, y);
}

void Table::DrawColums(HDC hdc, const int height)
{
	const int columnWidth = windowWidth / columsCount;
	for (int i = 1; i < columsCount; i++)
	{
		int x = i * columnWidth;
		DrawLine(hdc, x, 0, x, height, NULL);
	}
}

void Table::SetWindowSize(int height, int width)
{
	windowHeight = height;
	windowWidth = width;
}

void Table::DrawLine(HDC hdc, int x1, int y1, int x2, int y2, LPPOINT lppt)
{
	MoveToEx(hdc, x1, y1, lppt);
	LineTo(hdc, x2, y2);
}
